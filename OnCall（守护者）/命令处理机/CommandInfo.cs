﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace 命令处理机
{
    /// <summary>
    /// 命令处理信息
    /// </summary>
    public class CommandInfo
    {
        public static string NewLine = System.Environment.NewLine;

        public string Command { private set; get; }
        public string[] Args { private set; get; }
        public bool isSingleLineType { private set; get; }
        public Dictionary<string, object> Attach;

        /// <summary>
        /// 带参单行命令
        /// </summary>
        /// <param name="command">命令名称</param>
        /// <param name="args">参数列表</param>
        public CommandInfo(string command, string[] args)
        {
            Command = command.ToUpper();
            Args = args;
            isSingleLineType = true;
            Attach = null;
        }

        /// <summary>
        /// 无参单行命令
        /// </summary>
        /// <param name="command">命令名称</param>
        public CommandInfo(string command)
        {
            Command = command.ToUpper();
            Args = new string[0];
            isSingleLineType = true;
            Attach = null;
        }

        /// <summary>
        /// 带参多行命令
        /// </summary>
        /// <param name="command">命令名称</param>
        /// <param name="arg">参数</param>
        public CommandInfo(string command, string arg)
        {
            Command = command.ToUpper();
            Args = new string[1] { arg };
            isSingleLineType = false;
            Attach = null;
        }
    }
}
