﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Forms;

namespace 命令处理机
{
    public class 系统相关
    {
        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, uint msg, uint wParam, int lParam);
        [DllImport("user32.dll")]
        private static extern void LockWorkStation();

        /// <summary>
        /// 有bug
        /// </summary>
        /// <param name="info"></param>
        public static void 切换静音(CommandInfo info)
        {
            SendMessage((IntPtr)0xffff, 0x319, 0x200eb0, 0x80000);
        }

        /// <summary>
        /// 有bug
        /// </summary>
        /// <param name="info"></param>
        public static void 增大音量(CommandInfo info)
        {
            SendMessage((IntPtr)0xffff, 0x319, 0x30292, 0xa0000);
        }

        /// <summary>
        /// 有bug
        /// </summary>
        /// <param name="info"></param>
        public static void 减小音量(CommandInfo info)
        {
            SendMessage((IntPtr)0xffff, 0x319, 0x30292, 0x90000);
        }

        /// <summary>
        /// 显示屏开关
        /// </summary>
        /// <param name="info"></param>
        public static void 显示屏开关(CommandInfo info)
        {
            if (info.Args.Length > 0)
            {
                string arg = info.Args[0].ToUpper();
                if (arg == "OFF")
                {
                    SendMessage((IntPtr)0xffff, 0x0112, 0xF170, 0x2);
                }
                else if (arg == "ON")
                {
                    SendMessage((IntPtr)0xffff, 0x0112, 0xF170, -0x1);
                }
            }
            else
            {
                SendMessage((IntPtr)0xffff, 0x0112, 0xF170, 0x2);
            }
        }

        /// <summary>
        /// 锁闭系统
        /// </summary>
        /// <param name="info"></param>
        public static void 锁闭系统(CommandInfo info)
        {
            LockWorkStation();
        }

        /// <summary>
        /// 启动程序
        /// </summary>
        /// <param name="info"></param>
        public static void 启动程序(CommandInfo info)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = info.Args[0];
            Process process = new Process();
            process.StartInfo = startInfo;
            process.Start();
        }

        /// <summary>
        /// 系统提示
        /// </summary>
        /// <param name="info"></param>
        public static void 系统提示(CommandInfo info)
        {
            MessageBox.Show(info.Args[0]);
        }
    }
}
